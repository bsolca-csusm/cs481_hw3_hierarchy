# **CS481_HW3_hierarchy**

[CSUSM](https://www.csusm.edu/) - Introduction in Mobile Programming.
Hierarchy App in Xamarin. (Rotation in landscape is disable)

A simple application to see the result on two primary colors.

## What I learn

 - **OnAppearing/OnDisappearing** 
	 - Override a method and do somes tests to see reactions
	 - The main page use OnAppearing to load Application.Current.Properties
 - **Application.Current.Properties**
	 - Save properties in the application 
	 - Save object in the Properties
	 - TryGetValue usage to get the object
 - **Pass argument on a clicked button method:**
	 - See ButtonClicked in ColorMixPage
 - **Architecture**
	 - Use constructor parameters to reduce the number of pages (see screenshots below)
	 
Before:

![Screenshot from app](before.png)

After:

![Screenshot from app](after.png)

doc.v1.0