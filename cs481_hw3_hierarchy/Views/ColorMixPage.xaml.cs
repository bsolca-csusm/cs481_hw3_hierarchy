﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481_hw3_hierarchy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorMixPage : ContentPage
    {
        private ContentPage _leftPage;
        private ContentPage _rightPage;

        public ColorMixPage(EColors color)
        {
            InitializeComponent();
            Label.Text = "You choose " + color;
            switch (color)
            {
                case EColors.Red:
                    ButtonLeft.BackgroundColor = Color.Yellow;
                    ButtonRight.BackgroundColor = Color.Blue;
                    _leftPage = new ColorResultPage(EColors.Orange); // Mix with Yellow
                    _rightPage = new ColorResultPage(EColors.Purple); // Mix with Blue
                    break;
                case EColors.Blue:
                    ButtonLeft.BackgroundColor = Color.Red;
                    ButtonRight.BackgroundColor = Color.Yellow;
                    _leftPage = new ColorResultPage(EColors.Purple); // Mix with Red
                    _rightPage = new ColorResultPage(EColors.Green); // Mix with Yellow
                    break;
                case EColors.Yellow:
                    ButtonLeft.BackgroundColor = Color.Blue;
                    ButtonRight.BackgroundColor = Color.Red;
                    _leftPage = new ColorResultPage(EColors.Green); // Mix with Blue
                    _rightPage = new ColorResultPage(EColors.Orange); // Mix with Red
                    break;
                default:
                    ButtonLeft.BackgroundColor = Color.Transparent;
                    ButtonRight.BackgroundColor = Color.Transparent;
                    _leftPage = null;
                    _rightPage = null;
                    break; // Set to null to handle color setting error
            }
        }

        private async void ButtonClicked(object sender, EventArgs e)
        {
            string direction = ((Button)sender).BindingContext as string;

            if (_rightPage != null && direction == "Right")
                await Navigation.PushAsync(_rightPage);
            else if (_rightPage != null && direction == "Left")
                await Navigation.PushAsync(_leftPage);
            else
                await DisplayAlert("Unexpected error", "Contact support@hw3.io", "Damn");
        }
    }
}