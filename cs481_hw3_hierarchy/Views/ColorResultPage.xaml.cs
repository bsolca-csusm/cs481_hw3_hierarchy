﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481_hw3_hierarchy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorResultPage : ContentPage
    {
        private readonly MyColor _myColor = new MyColor();

        public ColorResultPage(EColors color)
        {
            InitializeComponent();
            switch (color)
            {
                case EColors.Orange:
                    _myColor.ColorName = "Orange";
                    _myColor.MyColorObj = Color.Orange;
                    break;
                case EColors.Purple:
                    _myColor.ColorName = "Purple";
                    _myColor.MyColorObj = Color.Purple;
                    break;
                case EColors.Green:
                    _myColor.ColorName = "Green";
                    _myColor.MyColorObj = Color.Green;
                    break;
                default:
                    _myColor.ColorName = "";
                    _myColor.MyColorObj = Color.Transparent;
                    break;
            }
            ButtonColor.BackgroundColor = _myColor.MyColorObj;
        }

        protected override void OnDisappearing()
        {
            Application.Current.Properties["LastColorName"] = _myColor.ColorName;
            Application.Current.Properties["LastColor"] = _myColor.MyColorObj;
            base.OnDisappearing();
        }

        private async void RootClicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}