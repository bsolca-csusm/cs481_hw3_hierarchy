﻿using Xamarin.Forms;

namespace cs481_hw3_hierarchy
{
    public enum EColors
    {
        Red,
        Blue,
        Yellow,
        Green,
        Purple,
        Orange,
    }
    class MyColor
    {
        public string ColorName { get; set; }
        public Color MyColorObj { get; set; }
    }
}
