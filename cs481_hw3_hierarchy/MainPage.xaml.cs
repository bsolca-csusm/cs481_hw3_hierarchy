﻿using System;
using System.ComponentModel;
using System.Net.Http.Headers;
using cs481_hw3_hierarchy.Views;
using Xamarin.Forms;

namespace cs481_hw3_hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Application.Current.Properties["LastColorName"] = "";
            Application.Current.Properties["LastColor"] = Color.DarkGray;
            DescriptionLabel.Text = "";
        }

        // override the OnAppearing function to display an alert
        protected override void OnAppearing()
        {
            DisplayAlert("How to", "Tap on a color to select it", "OK");
            object _color;
            if (Application.Current.Properties.TryGetValue("LastColor", out _color))
                DescriptionLabel.TextColor = (Color) _color;
            if (Application.Current.Properties["LastColorName"].ToString() != "")
                DescriptionLabel.Text = "Last mixed color was " + Application.Current.Properties["LastColorName"].ToString();
            base.OnAppearing();
        }

        // Clicked methods start
        private async void BluePageClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ColorMixPage(EColors.Blue));
        }

        private async void RedPageClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ColorMixPage(EColors.Red));
        }

        private async void YellowPageClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ColorMixPage(EColors.Yellow));
        }

        // Clicked method end
    }
}